import * as plugins from './projectinfo.plugins.js';

// direct access to classes
export * from './projectinfo.classes.git.js';
export * from './projectinfo.classes.npm.js';
export * from './projectinfo.classes.projectinfo.js';

// npm
import { ProjectinfoNpm } from './projectinfo.classes.npm.js';

// quick functions

/**
 * gets the name from package.json in a specified directory
 */
export let getNpmNameForDir = function (cwdArg) {
  let localNpm = new ProjectinfoNpm(cwdArg);
  if (localNpm.status === 'ok') {
    return localNpm.name;
  }
};
